## Testbed for macOS pipeline on GitLab
Looks like it's currently still in Beta; although called Open Beta, access is currently restricted. Cf.
- https://gitlab.com/gitlab-com/macos-buildcloud-runners-beta/-/issues
- https://gitlab.com/gitlab-com/macos-buildcloud-runners-beta/-/issues/19#note_536846933

Tests halted until general availability.
