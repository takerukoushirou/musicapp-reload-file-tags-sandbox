//@osa-lang:JavaScript
/*
 * Music.app Reload File Tags
 *
 * Copyright (C) 2022, Michael Maier
 */

'use strict';

// Run event, invoked on script invocation.
var run = (() => {

    /**
     * Measures time with support for markers. A stopwatch object is immediately
     * active after instantiation.
     */
    class Stopwatch {
        #startDate;
        #lastMarkDate;

        constructor() {
            this.startDate = new Date();
        }

        /**
         * Returns the date of the last marker.
         */
        get lastMark() {
            return this.lastMarkDate ?? this.startDate;
        }

        /**
         * Sets a new marker and returns the duration since the last marker.
         * @returns
         * Duration in milliseconds since last marker or start of the stopwatch,
         * if no marker has been created yet.
         */
        mark() {
            let mark = new Date();
            let duration = mark - this.lastMark;

            this.lastMarkDate = mark;

            return duration;
        }

        /**
         * Sets a new marker and returns the duration since the last marker in
         * seconds (with precision 2).
         * @returns
         * Duration in seconds since last marker or start of the stopwatch, if
         * no marker has been created yet.
         */
        markInSeconds() {
            return (this.mark() / 1000).toPrecision(2);
        }

        /**
         * Resets the start date to now.
         * @returns
         * Reference to this instance.
         */
        reset() {
            this.startDate = new Date();
            this.lastMarkDate = null;

            return this;
        }

        /**
         * Gets the duration between now and the start date of the stopwatch in
         * milliseconds.
         */
        get duration() {
            return new Date() - this.startDate;
        }

        /**
         * Gets the duration between now and the start date of the stopwatch in
         * seconds (with precision 2).
         */
        get durationInSeconds() {
            return (this.duration / 1000).toPrecision(2);
        }
    }


    /**
     * Represents a music track.
     *
     * Bridges access to an application track object with implicit caching of
     * loaded properties to speed up further access, e.g. during a sort operation.
     */
    class Track {
        #_item;
        #_album;
        #_albumArtist;
        #_artist;
        #_bitRate;
        #_discNumber;
        #_kind;
        #_name;
        #_sampleRate;
        #_sortAlbum;
        #_sortAlbumArtist;
        #_sortArtist;
        #_sortByArtist;
        #_sortByAlbum;
        #_trackNumber;

        /**
         * Initialises a new Track instance from an application track object.
         * @param item
         * Application track object to read and cache data from.
         */
        constructor(item) {
            this._item = item;
        }

        get album() {
            return this._album ?? (this._album = this._item.album());
        }

        get albumArtist() {
            return this._albumArtist ?? (this._albumArtist = this._item.albumArtist());
        }

        get artist() {
            return this._artist ?? (this._artist = this._item.artist());
        }

        /**
         * Numeric bitrate of track in kbps.
         */
        get bitRate() {
            return this._bitRate ?? (this._bitRate = this._item.bitRate());
        }

        get discNumber() {
            return this._discNumber ?? (this._discNumber = this._item.discNumber());
        }

        get kind() {
            return this._kind ?? (this._kind = this._item.kind());
        }

        get name() {
            return this._name ?? (this._name = this._item.name());
        }

        /**
         * Numeric sample rate of track in Hz.
         */
        get sampleRate() {
            return this._sampleRate ?? (this._sampleRate = this._item.sampleRate());
        }

        get sortAlbum() {
            return this._sortAlbum ?? (this._sortAlbum = this._item.sortAlbum());
        }

        get sortAlbumArtist() {
            return this._sortAlbumArtist ?? (this._sortAlbumArtist = this._item.sortAlbumArtist());
        }

        get sortArtist() {
            return this._sortArtist ?? (this._sortArtist = this._item.sortArtist());
        }

        /**
         * Transformed artist name used for sorting tracks.
         */
        get sortByArtist() {
            if (typeof this._sortByArtist === "undefined") {
                let artist = this.sortAlbumArtist;

                if (!artist) {
                    artist = this.albumArtist;

                    if (!artist) {
                        artist = this.artist;
                    }
                }

                this._sortByArtist = artist.toUpperCase();
            }

            return this._sortByArtist;
        }

        /**
         * Transformed album name used for sorting tracks.
         */
        get sortByAlbum() {
            if (typeof this._sortByAlbum === "undefined") {
                let album = this.sortAlbum;

                if (!album) {
                    album = this.album;
                }

                this._sortByAlbum = album.toUpperCase();
            }

            return this._sortByAlbum;
        }

        get trackNumber() {
            return this._trackNumber ?? (this._trackNumber = this._item.trackNumber());
        }

        refresh() {
            return this._item.refresh;
        }

        /**
         * Compares two tracks for sorting. Designed to be used in Array.sort().
         */
        static compare(a, b) {
            const artistComparison = a.sortByArtist.localeCompare(b.sortByArtist);

            if (artistComparison == 0) {
                const albumComparison = a.sortByAlbum.localeCompare(b.sortByAlbum);

                if (albumComparison == 0) {
                    const idA = a.discNumber * 10000 + a.trackNumber;
                    const idB = b.discNumber * 10000 + b.trackNumber;

                    return idA - idB;
                }

                return albumComparison;
            }

            return artistComparison;
        }
    }

    /**
     * Startup class, wraps the script main function and helpers.
     */
    class Startup {
        #app;
        #appProperties;

        constructor() {
            this.app = Application.currentApplication();
            this.app.includeStandardAdditions = true;

            // Cache properties, as direct access to properties yields undefined (JXA shortcoming).
            // If the script is not hosted, access to properties() or other properties will throw
            // a "Message not understood" error; in this case, a placeholder properties object will
            // be used instead.
            try {
                this.appProperties = this.app.properties();
            }
            catch (e) {
                // Running as a standalone applet, use static properties.
                this.appProperties = { name: "Reload File Tags" };
            }
        }

        /**
         * Name of the currently running application (applet or host).
         */
        get appName() {
            return this.appProperties["name"];
        }

        /**
         * Version of the currently running application (applet or host).
         */
        get appVersion() {
            return this.appProperties["version"];
        }

        /**
         * Retrieves the Music application instance. This does not launch the
         * application, if it is not already running.
         */
        get musicApp() {
            if (this.runningInsideMusic) {
                // Use current application instance if already running within
                // Music, as otherwise the host application will be indefinitely
                // blocked.
                return this.app;
            }

            return Application("Music");
        }

        /**
         * Determines whether the script is hosted within Music.app.
         */
        get runningInsideMusic() {
            return this.appName === "Music";
        }

        /**
         * Fetches tracks from Music.app.
         * @returns
         * Array of Track instances on success, otherwise undefined.
         */
        fetchTracks() {
            const music = this.musicApp;

            if (!music.running()) {
                try {
                    this.app.displayAlert("Launch Music application?", {
                        as: "informational",
                        message: "Music is currently not running, but is required to access music tracks.",
                        buttons: ["Launch Music", "Cancel"],
                        defaultbutton: 1,
                        cancelButton: 2
                    });
                }
                catch {
                    // Use cancelled dialog, return undefined tracklist.
                    return;
                }

                // Music.app is automatically launched during first interaction.
            }

            let selectedTracks = music.selection();

            if (!this.runningInsideMusic && !selectedTracks.length) {
                // Only default to full tracklist if called as an applet.
                selectedTracks = music.tracks();
            }

            const tracks = [];

            for (let track of selectedTracks) {
                if (track.class() === 'fileTrack') {
                    tracks.push(new Track(track));
                }
            }

            return tracks;
        }

        main() {
            const stopwatch = new Stopwatch();

            // Retrieve array of tracks to process.
            Progress.totalUnitCount = -1;
            Progress.description = "Loading tracks...";
            console.log(`⏳ Getting track list from Music.app...`);
            const tracks = this.fetchTracks();

            if (typeof tracks === "undefined") {
                console.log(`🛑 Cancelled.`);
                return;
            }

            const totalTracks = tracks.length;
            console.log(`✅ Found ${totalTracks} tracks in ${stopwatch.mark()} ms.`);
            Progress.totalUnitCount = 0;

            // Sort tracks by album.
            Progress.totalUnitCount = -1;
            Progress.description = `Sorting ${totalTracks.toLocaleString()} tracks...`;
            console.log(`⏳ Sorting tracks by album...`);
            tracks.sort(Track.compare);
            console.log(`✅ Sorted tracks in ${stopwatch.mark()} ms.`);
            Progress.totalUnitCount = 0;

            // Refresh tracks in sorted order.
            let currentAlbum = null;
            let index = 0;

            Progress.totalUnitCount = totalTracks;
            Progress.completedUnitCount = 0;
            Progress.description = "Refreshing tracks...";

            let progressUpdate = (() => {
                let lastUpdate = new Date();

                return () => {
                    let now = new Date();

                    if (now - lastUpdate > 5000) {
                        console.log(`${(index / totalTracks * 100).toPrecision(2)}% completed`);
                        lastUpdate = now;
                    }
                };
            })();

            for (let track of tracks) {
                let trackAlbum = `${track.sortByArtist} – ${track.sortByAlbum}`;

                if (currentAlbum !== trackAlbum) {
                    console.log(`💿 ${trackAlbum}`);
                    currentAlbum = trackAlbum;
                }

                Progress.additionalDescription =
                    `Track ${(index + 1).toLocaleString()} of ${totalTracks.toLocaleString()}:` +
                    ` ${trackAlbum} ${track.discNumber}-${track.trackNumber}`;
                console.log(
                    `   🎵 ${track.discNumber}-${track.trackNumber}. ${track.artist} – ${track.name}` +
                    `  (${track.kind}, ${track.sampleRate} Hz, ${track.bitRate} kbps)`
                );

                track.refresh();

                index++;
                Progress.completedUnitCount = index;
                progressUpdate();
            }

            const refreshTime = stopwatch.markInSeconds();
            const totalTime = stopwatch.durationInSeconds;
            console.log();
            console.log(`✅ Finished processing ${totalTracks} tracks in ${totalTime} s (refresh took ${refreshTime} s).`);

            this.app.displayNotification(
                `Refresh took ${refreshTime} s, total time ${totalTime} s.`, {
                withTitle: "Refresh Tags",
                subtitle: `Completed processing ${totalTracks} music tracks`
            });
        }
    }

    return () => {
        try {
            return new Startup().main();
        }
        catch (e) {
            var app = Application.currentApplication();
            app.includeStandardAdditions = true;

            app.displayAlert("Reload File Tags Error", { message: `An unhandled error ocurred: ${e.message}` });

            throw e;
        }
    };
})();